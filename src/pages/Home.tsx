import React, {useState, useCallback, useEffect} from 'react';
import {
  Layout,
  Page,
  FooterHelp,
  Card,
  Link,
  Button,
  FormLayout,
  TextField,
  AccountConnection,
  ChoiceList,
  SettingToggle,
} from '@shopify/polaris';
import {ImportMinor} from '@shopify/polaris-icons';
import { Favorites } from "../redux/cities";
import axios, { CancelTokenSource } from "axios";
import { query } from "../services/whether";
import { List } from '@shopify/polaris';
import { add } from '../redux/cities';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

interface AccountProps {
  onAction(): void;
}

let cancelToken: CancelTokenSource | undefined;

export const Home = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const [searchValue, setSearchValue] = useState("");
  const [cities, setCities] = useState<Favorites[]>([]);
  const [loading, setLoading] = useState(false);

  const fetch = async (value: string) => {
    try {
      cancelToken && cancelToken.cancel();
      cancelToken = axios.CancelToken.source();

      setLoading(true);
      const data = await query(value, cancelToken.token);
      setLoading(false);

      setCities([data])
    } catch (error) {
      console.log(error);
      setCities([])
      setLoading(false);
    }
  };

  const onChange = (value: string) => {
    setSearchValue(value);
    if (!value) return;
    fetch(value)
  };

  const goToFavorites = () => history.push('/favorites')

  const onClick = () =>  {setCities([]); dispatch(add(cities[0]))}

  useEffect(() => {
    fetch('Moscow');
  }, [])

  return (
      <Layout>
        <Button onClick={goToFavorites}>go to favorites</Button>
        <Layout.AnnotatedSection
          title="Search for city"
          description="input your city"
        >
         <TextField
                  value={searchValue}
                  label="City"
                  placeholder=""
                  onChange={onChange}
                />
        </Layout.AnnotatedSection>

        <Layout.AnnotatedSection
          title="Cities"
          description="weather in city"
        >
           {loading ? '...loading' : cities.length ? cities.map((item, i) => <Card key={i} sectioned>
            <FormLayout>
            <List>
    <List.Item>base - {item.base}</List.Item>
    <List.Item>clouds - {item.clouds.all}</List.Item>
    <List.Item>name - {item.name}</List.Item>
    <List.Item>temp - {item.main.temp}</List.Item>
    <List.Item>feels like - {item.main.feels_like}</List.Item>
    <List.Item>temp - {item.weather[0].main}</List.Item>
  </List>
              <Button primary onClick={onClick}>add to favorites</Button>
            </FormLayout>
          </Card>) : 'nothing found'}
        </Layout.AnnotatedSection>
      </Layout>
  );
}
