import React, {useState, useCallback, useEffect} from 'react';
import {
  Layout,
  Page,
  FooterHelp,
  Card,
  Link,
  Button,
  FormLayout,
  TextField,
  AccountConnection,
  ChoiceList,
  SettingToggle,
} from '@shopify/polaris';
import {ImportMinor} from '@shopify/polaris-icons';
import cities, { Favorites, selectFavorites } from "../redux/cities";
import axios, { CancelTokenSource } from "axios";
import { List } from '@shopify/polaris';
import { removeById, clearAll } from '../redux/cities'
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

interface AccountProps {
  onAction(): void;
}

let cancelToken: CancelTokenSource | undefined;

export const FavoritesComponent = () => {
    const history = useHistory();
    const favorites = useSelector(selectFavorites);
    const dispatch = useDispatch();

  const remove = (name: string) =>  dispatch(removeById(name))
  const clear = () =>  dispatch(clearAll())

  const goToHome = () =>  history.push('/')

  return (
      <Layout>
        <Button onClick={goToHome}>go to Home</Button>
        <Button onClick={clear}>clear all</Button>

        <Layout.AnnotatedSection
          title="Cities"
          description="weather in city"
        >
           {favorites.length ? favorites.map((item: any, i: number) => <Card key={i} sectioned>
            <FormLayout>
            <List>
    <List.Item>base - {item.base}</List.Item>
    <List.Item>clouds - {item.clouds.all}</List.Item>
    <List.Item>name - {item.name}</List.Item>
    <List.Item>temp - {item.main.temp}</List.Item>
    <List.Item>feels like - {item.main.feels_like}</List.Item>
    <List.Item>temp - {item.weather[0].main}</List.Item>
  </List>
              <Button primary onClick={() => remove(item.name)}>remove from favorites</Button>
            </FormLayout>
          </Card>) : 'nothing added'}
        </Layout.AnnotatedSection>
      </Layout>
  );
}
