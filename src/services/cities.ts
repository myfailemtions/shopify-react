import axios from './axios';
import { CancelToken } from 'axios';

export interface SearchCitiesRequest {}

export const query = (data: SearchCitiesRequest, baseURL?: string | undefined,cancelToken?: CancelToken) =>
    axios<any>({
        url: '/admin/video/comment/query',
        cancelToken,
        baseURL,
        data
    });
