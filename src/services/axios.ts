/* eslint-disable import/no-anonymous-default-export */
import axios, { AxiosPromise, AxiosRequestConfig } from "axios";

const axiosInstance = axios.create();
axiosInstance.defaults.baseURL = "https://api.openweathermap.org";
axiosInstance.defaults.method = "GET";
axiosInstance.interceptors.response.use(
  (config) => config.data,
  (config) => Promise.reject(config.response)
);

export default <T = void>(config: AxiosRequestConfig) => {
  const request: AxiosPromise<T> = axiosInstance({
    ...config,
    headers: {}
  });

  return (request as any) as Promise<T>;
};
